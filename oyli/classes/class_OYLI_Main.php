<?php
namespace oyli\theme;

/**
* This class init all theme methods
*/
class OYLI
{
    public function __construct()
    {
        add_action( 'wp_print_styles', array($this,'add_styles') );
        add_action( 'wp_footer', array($this,'add_counters') );
    }

    public static function add_styles()
    {
        wp_enqueue_style('oyli', get_stylesheet_directory_uri().'/oyli/css/oyli.css?v=2');
    }

    public static function add_counters()
    {
        $fh = fopen(get_stylesheet_directory() . '/oyli/counters.txt','r');
            while ($line = fgets($fh)) {
                echo($line);
            }
        fclose($fh);
    }


}

new OYLI();
